package gr.pgetsos;

import gr.pgetsos.helpers.LoggerHelperClass;
import org.junit.Assert;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by PGETSOS on 04/01/2017. Γκετσόπουλος Πέτρος 3130041
 */
class UtilityClassTestHelper {
    private static final Logger logger = LoggerHelperClass.getNewLogger(UtilityClassTestHelper.class);

    private UtilityClassTestHelper(){}

    static void assertUtilityClassWellDefined(final Class<?> clazz) throws TestFailedException {
        try {
            Assert.assertTrue("class must be final",
                    Modifier.isFinal(clazz.getModifiers()));
            Assert.assertEquals("There must be only one constructor", 1,
                    clazz.getDeclaredConstructors().length);
            final Constructor<?> constructor = clazz.getDeclaredConstructor();
            if (constructor.isAccessible() ||
                    !Modifier.isPrivate(constructor.getModifiers())) {
                Assert.fail("constructor is not private");
            }
            constructor.setAccessible(true);
            constructor.newInstance();
            constructor.setAccessible(false);
            for (final Method method : clazz.getMethods()) {
                if (!Modifier.isStatic(method.getModifiers())
                        && method.getDeclaringClass().equals(clazz)) {
                    Assert.fail("there exists a non-static method:" + method);
                }
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Wrong helper class initialization?", e);
            throw new TestFailedException(e.getMessage());
        }
    }
}
