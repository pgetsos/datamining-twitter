package gr.pgetsos;

/**
 * Created by PGETSOS on 03/01/2017. Γκετσόπουλος Πέτρος 3130041
 */

class TestFailedException extends Exception {
    TestFailedException(String msg){
        super(msg);
    }
}
