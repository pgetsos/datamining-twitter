package gr.pgetsos;

import gr.pgetsos.helpers.GreekStemmer;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by PGETSOS on 04/01/2017. Γκετσόπουλος Πέτρος 3130041
 */
public class GreekStemmerTest {
    @Test
    public void stem() {
        assertEquals("Ο ΠΕΤΡ ΕΙΝ ΦΙΛ ΚΑΛ ΚΑΙ Η ΜΑΡ ΕΠΙΣ ΕΙΝ", GreekStemmer.stemString("Ο ΠΕΤΡΟΣ ΕΙΝΑΙ ΦΙΛΟΣ ΚΑΛΟΣ ΚΑΙ Η ΜΑΡΙΑ ΕΠΙΣΗΣ ΕΙΝΑΙ"));
    }

    @Test
    public void testClass() throws TestFailedException {
        UtilityClassTestHelper.assertUtilityClassWellDefined(GreekStemmer.class);
    }
}