package gr.pgetsos;

import gr.pgetsos.helpers.TweetCleaner;
import org.junit.Test;
import twitter4j.Status;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by PGETSOS on 04/01/2017. Γκετσόπουλος Πέτρος 3130041
 */
public class TweetCleanerTest {
    @Test
    public void cleanData() throws TestFailedException {
        String tweet1 = "I c RT @iamFink: @SamanthaSpice that's my excited face and my regular face. The expression never changes.";
        String tweet2 = "RT @AstrologyForYou: #Gemini recharges through regular contact with people of like mind, and social involvement that allows expression of their ideas";
        String tweet3 = "New comment by diego.bosca: Re: Re: wrong regular expression? http://t.co/4KOb94ua";
        String tweet4 = "New comment by diego.bosca: Re: Re: wrong regular expression? ";

        String cleanTweet1 = "I c RT that s my excited face and my regular face The expression never changes";
        String cleanTweet2 = "RT recharges through regular contact with people of like mind and social involvement that allows expression of their ideas";
        String cleanTweet3 = "New comment by diego bosca Re Re wrong regular expression";

        Status status1 = mock(Status.class);
        when(status1.getText()).thenReturn(tweet1);
        Status status2 = mock(Status.class);
        when(status2.getText()).thenReturn(tweet2);
        Status status3 = mock(Status.class);
        when(status3.getText()).thenReturn(tweet3);
        Status status4 = mock(Status.class);
        when(status4.getText()).thenReturn(tweet4);

        List<Status> tweets = new ArrayList<>(Arrays.asList(status1, status2, status3, status4));
        Set<String> cleanTweets = new HashSet<>(Arrays.asList(cleanTweet1.toUpperCase(new Locale("el")), cleanTweet2.toUpperCase(new Locale("el")), cleanTweet3.toUpperCase(new Locale("el"))));
        try {
            TweetCleaner.initStopwords();
        } catch (ApplicationFailedException e){
            throw new TestFailedException(e.getMessage());
        }

        assertEquals(cleanTweets, TweetCleaner.cleanData(tweets).keySet());
    }

    @Test
    public void cleanTweet() {
        assertEquals("I really love that shirt at".toUpperCase(new Locale("el")), TweetCleaner.cleanTweet("@peter I really love that shirt at #Macy. http://bit.ly//WjdiW4"));
    }

    @Test
    public void testClass() throws TestFailedException {
        UtilityClassTestHelper.assertUtilityClassWellDefined(TweetCleaner.class);
    }
}