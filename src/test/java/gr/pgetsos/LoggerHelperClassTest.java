package gr.pgetsos;

import gr.pgetsos.helpers.LoggerHelperClass;
import org.junit.Test;

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

/**
 * Created by PGETSOS on 04/01/2017. Γκετσόπουλος Πέτρος 3130041
 */
public class LoggerHelperClassTest {
    @Test
    public void getNewLogger() {
        Logger logger = LoggerHelperClass.getNewLogger(this.getClass());
        assertEquals(Level.FINE, logger.getLevel());
        assertEquals(this.getClass().getName(), logger.getName());
    }

    @Test
    public void testClass() throws TestFailedException {
        UtilityClassTestHelper.assertUtilityClassWellDefined(LoggerHelperClass.class);
    }

}