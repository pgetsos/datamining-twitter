/*
 * Created by PGETSOS on 03/01/2017. Γκετσόπουλος Πέτρος 3130041
 */

package gr.pgetsos;

/**
 * Custom Exception to close Application gracefully after a serious Exception
 * instead of calling System.exit() or similar functions
 */
public class ApplicationFailedException extends Exception {

    /**
     * A simple constructor that calls the parent method for error printing
     * @param msg the error message to be printed
     */
    public ApplicationFailedException(Throwable msg){
        super(msg);
    }
}
