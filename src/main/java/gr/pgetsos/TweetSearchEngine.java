/*
 * Created by PGETSOS on 26/10/2016. Γκετσόπουλος Πέτρος 3130041
 */

package gr.pgetsos;

import gr.pgetsos.graphs.Bar;
import gr.pgetsos.graphs.Bar2;
import gr.pgetsos.graphs.DataSetObject;
import gr.pgetsos.helpers.EmotionAnalyzer;
import gr.pgetsos.helpers.LoggerHelperClass;
import gr.pgetsos.helpers.TweetCleaner;
import org.jfree.ui.RefineryUtilities;
import twitter4j.Query;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.QueryResult;
import twitter4j.TwitterException;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.*;

class TweetSearchEngine {

    private static final Logger LOGGER = LoggerHelperClass.getNewLogger(TweetSearchEngine.class);
    private static final String HASHTAG_ND = "#ND";
    private static final String HASHTAG_SYRIZA = "#SYRIZA";
    private static final String MENTION_TSIPRAS = "@atsipras";
    private static final String MENTION_MITSOTAKIS = "@kmitsotakis";
    private Date beginning;
    private long lastID = Long.MAX_VALUE;
    private Twitter twitter;
    private DatabaseCreation db;

    /**
     * Call the methods it needs for every different input
     * @param input the choice of the user
     * @param username the username for the DataBase
     * @param password the password for the DataBase
     * @throws ApplicationFailedException if anything goes wrong
     */
    void getTweets(int input, String username, String password) throws ApplicationFailedException{
        initialize(username, password);
        if(input==1 || input == 2) {
            db.createTable(input);
            db.createTableForWords(input);
            TweetCleaner.initStopwords();
            EmotionAnalyzer.initDictionaries();
            indexTweets(HASHTAG_ND);
            indexTweets(HASHTAG_SYRIZA);
            indexTweets(MENTION_TSIPRAS);
            indexTweets(MENTION_MITSOTAKIS);
        } else{
            getStats(input);
        }
        db.closeCon();
    }

    /**
     * Initialize a Twitter object, along
     * with a DatabaseCreation object, which
     * is responsible for Database management
     */
    private void initialize(String username, String password) throws ApplicationFailedException{
        TwitterFactory tf = new TwitterFactory();
        twitter = tf.getInstance();
        setDates();
        db = new DatabaseCreation(username, password);
    }

    /**
     * Get a Calendar object with the yesterday's day
     */
    private void setDates(){
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DAY_OF_MONTH, -7);
        beginning = cal.getTime();
    }

    /**
     * Get a Date object and format it in the way
     * Twitter Search API understands it
     * @param date is the Date object we want formatted
     * @return a string from the above Date
     */
    private String formattedDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    /**
     * The method that is responsible for the queries.
     * We create a query for only the last week, Greek locale,
     * without retweets and starting from the most recent one, and
     * using the last ID we can eliminate duplicates
     * @param searchTerm is the String we search for
     * @return an ArrayList with all (up to 100) tweets of the day
     */
    private List<Status> getTweetsSearch(String searchTerm) throws ApplicationFailedException{
        try {
            // Avoid the ReTweets
            Query query = new Query(searchTerm+" +exclude:retweets");
            // Search in a specific time frame
            query.setSince(formattedDate(beginning));
            // Only Greek tweets (as the HashTag may was used for other reasons by foreign Tweeps)
            query.lang("el");
            // Maximum tweets per search
            query.count(100);
            // Search only for the most recent tweets (easier to check for duplicate IDs by setting MaxID)
            query.setResultType(Query.ResultType.recent);
            query.setMaxId(lastID-1);
            // The actual query
            QueryResult result = twitter.search(query);
            List<Status> tweets = new ArrayList<>(result.getTweets());
            // Avoid duplicate tweets by marking the Tweet IDs of last search
            if(!tweets.isEmpty()) {
                lastID = tweets.get(tweets.size() - 1).getId();
            }
            return tweets;
        } catch (TwitterException te) {
            LOGGER.log(Level.SEVERE, "Failed to search tweets", te);
            throw new ApplicationFailedException(te);
        }
    }

    /**
     * This method is responsible for the management of the Tweets.
     * Resets the lastID, in case it was changed by a previous search.
     * Then, calls getTweetsSearch to return the Tweets of a specific term,
     * for the whole of the previous week (until it returns 0 Tweets).
     * Continue as long as there are 100 returned Tweets which is the
     * maximum for each query. Then add every individual returned list
     * in a ArrayList and the final one save it in a Database.
     * @param searchTerm is the term we search for
     */
    private void indexTweets(String searchTerm) throws ApplicationFailedException{
        int queries = 0;
        int originalSize = 100;
        // For the next queries, reset the lastID value
        lastID = Long.MAX_VALUE;
        LOGGER.fine("Searching for: "+searchTerm);

        List<Status> tweets = new ArrayList<>();

        // This will look for up as long as there are returned Tweets
        while (originalSize == 100) {
            List<Status> tempTweets = getTweetsSearch(searchTerm);
            /*
             * 180 queries in 15 minutes mean 1 query every 5 seconds
             * so we sleep the thread for that amount and the program can run
             * continuously without having to sleep for the whole 15 minutes,
             * or experiencing other errors due to the limit (e.x. empty lists
             * while there are still more Tweets to be returned).
             */
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            queries++;
            // If we get an empty result, there is nothing more to add in the ArrayList, break while loop
            if (tempTweets.isEmpty())
                break;
            originalSize = tempTweets.size();
            tweets.addAll(tempTweets);
            /*
             * 100 tweets mean there are (probably) more, so change the
             * lastID to prevent duplicates. If it's less than 100,
             * no more Tweets exist, so continue to Database insertion.
            */
            if (originalSize == 100) {
                lastID = tweets.get(tweets.size() - 1).getId();
                if (queries % 3 == 0) {
                    LOGGER.fine("Still working on it, please wait.... Just completed query " + queries);
                }
            }
        }
        LOGGER.fine("Gathered " + tweets.size() + " tweets in " + queries + " queries");
        String returned = db.saveToTable(searchTerm, TweetCleaner.cleanData(tweets));
        LOGGER.fine("Database insertion " + returned);
        returned = db.saveToTableFrequencies(searchTerm);
        LOGGER.fine("Database insertion " + returned);
        LOGGER.fine("\n***********************************\n");
    }

    /**
     * This method is responsible for showing up various stats.
     * It checks if a table with Tweets has been initialized first.
     * Then, it prints the contents of the ArrayList it receives
     * from the method in DatabaseCreation responsible for these, according
     * the choice the user made (small/detailed and category of the latter).
     */
    private void getStats(int input) {
        if(!db.checkTableExistence()){
            LOGGER.log(Level.INFO, "You haven't downloaded Tweets yet to analyze!");
            return;
        }
        if(input == 3){
            List<String> stats = db.getStats(HASHTAG_ND, HASHTAG_SYRIZA, MENTION_TSIPRAS, MENTION_MITSOTAKIS);
            stats.forEach(LOGGER::fine);
            LOGGER.fine("\n***********************************\n");
        }
        else if(input == 7){
            popChartFor(MENTION_TSIPRAS);
            popChartFor(MENTION_MITSOTAKIS);
            popChartFor(HASHTAG_SYRIZA);
            popChartFor(HASHTAG_ND);
        }
        else if(input == 6){
            String[] chartVals = {"Tweets per Term Chart", "Term", "Tweets"};
            List<DataSetObject> dataSet = db.getWStats();
            Bar2 demo = new Bar2("Weekly Tweet Chart Per Term", chartVals, dataSet, 1);
            demo.pack();
            RefineryUtilities.centerFrameOnScreen(demo);
            demo.setVisible(true);
        }
        else if(input == 8){
            String[] chartVals = {"Positive Average per Term Weekly Chart", "Term", "Tweets"};
            List<DataSetObject> dataSet = db.getWPosStats();
            Bar2 demo = new Bar2("Positive Median Chart Per Term", chartVals, dataSet, 0);
            demo.pack();
            RefineryUtilities.centerFrameOnScreen(demo);
            demo.setVisible(true);
        }
        else if(input == 9){
            popChartDeviationFor();
        }
    }

    /**
     * This method is responsible for showing up a population chart
     * for each term it receives, per day of the last week. The data
     * are provided by a method in DatabaseCreation class.
     */
    private void popChartFor(String term){
        String[] chartVals = {"Pos/Neg Population Chart of " + term , "Term", "Tweets"};
        List<DataSetObject> dataSet = db.getStats(term);
        Bar demo = new Bar("Population Chart Per Day", chartVals, dataSet);
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }

    /**
     * This method is responsible for showing up a population chart
     * of the Standard Deviation for each term for the last week. It
     * calls the calculateDeviation method below once for every term, and
     * then calls the chart class with the full data.
     */
    private void popChartDeviationFor(){
        String[] chartVals = {"Standard Deviation" , "Term", "SD"};
        List<DataSetObject> dataSet = new ArrayList<>();
        dataSet.add(calculateDeviation(HASHTAG_SYRIZA));
        dataSet.add(calculateDeviation(HASHTAG_ND));
        dataSet.add(calculateDeviation(MENTION_TSIPRAS));
        dataSet.add(calculateDeviation(MENTION_MITSOTAKIS));
        Bar demo = new Bar("Standard Deviation Per Term (Last 7 days)", chartVals, dataSet);
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }

    /**
     * This method is responsible for calculating the Standard Deviation
     * of a term. The data are provided by a method in DatabaseCreation class
     * that returns the Positive and Negative Tweets of a term for each day
     * of the pass 7 days.
     */
    private DataSetObject calculateDeviation(String term){
        List<DataSetObject> dataSet = db.getStats(term);
        double totalPos = 0;
        double totalNeg = 0;
        double totalSqPos = 0;
        double totalSqNeg = 0;
        for (DataSetObject ds : dataSet) {
            totalPos += ds.getPositive();
            totalNeg += ds.getNegative();
        }
        double avgPos = totalPos/7;
        double avgNeg = totalNeg/7;
        for (DataSetObject ds : dataSet) {
            totalSqPos =+ Math.pow(ds.getPositive()-avgPos, 2);
            totalSqNeg =+ Math.pow(ds.getNegative()-avgNeg, 2);
        }
        return new DataSetObject(term, (double) Math.round(Math.sqrt(totalSqPos/7)* 100) / 100, (double) Math.round(Math.sqrt(totalSqNeg/7)* 100) / 100);
    }
}