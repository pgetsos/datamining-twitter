/*
 * Created by PGETSOS on 06/01/2017. Γκετσόπουλος Πέτρος 3130041
 */

package gr.pgetsos.helpers;

import gr.pgetsos.ApplicationFailedException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

final class XLSReader {

    private static final Logger LOGGER = LoggerHelperClass.getNewLogger(XLSReader.class);

    /**
     * Private empty constructor of Helper Class
     */
    private XLSReader(){}

    /**
     * Given a file(XLS) path, read it and save all words in a Set
     * Uses the Apache POI library for reading
     * @param file the path of the file to be read
     * @return a Set with all the words of the given file
     * @throws ApplicationFailedException if file reading fails
     */
    static Set<String> getPosNegWords(String file) throws ApplicationFailedException {
        Set<String> words = new HashSet<>(100);
        try (POIFSFileSystem fs = new POIFSFileSystem(XLSReader.class.getResourceAsStream("/"+file))){
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            Cell cell;

            Iterator<Row> iter = sheet.rowIterator();
            //Skipping first row
            Row row = iter.next();
            LOGGER.finer("Reading words of category: "+row.getCell(2).getStringCellValue());
            while (iter.hasNext()){
                row = iter.next();
                cell = row.getCell(2);
                if(cell==null)
                    continue;
                words.add(cell.getStringCellValue());
            }
        }
        catch(IOException ioe) {
            LOGGER.log(Level.SEVERE, "Error reading the file", ioe);
            throw new ApplicationFailedException(ioe);
        }
        return words;
    }
}
