/*
 * Created by PGETSOS on 04/01/2017. Γκετσόπουλος Πέτρος 3130041
 */

package gr.pgetsos.helpers;

import java.util.logging.*;

public final class LoggerHelperClass {

    /**
     * Private empty constructor of Helper Class
     */
    private LoggerHelperClass(){}

    /**
     * Return a new Logger object for a specific class
     * and specific attributes
     * @param c the class to be used in Logger creation
     * @return the new Logger
     */
    public static Logger getNewLogger(Class c){
        Logger logger = Logger.getLogger(c.getName());
        Handler handlerObj = new ConsoleHandler();
        handlerObj.setLevel(Level.FINE);
        handlerObj.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                if(record.getThrown() != null)
                    return record.getMessage() +
                            "\n*********************\n" +
                            record.getThrown().getMessage() +
                            "\n*********************\n";
                else return record.getMessage() + "\n";
            }
        });
        logger.addHandler(handlerObj);
        logger.setLevel(Level.FINE);
        logger.setUseParentHandlers(false);
        return logger;
    }
}