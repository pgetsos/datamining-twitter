/*
 * Created by PGETSOS on 04/01/2017. Γκετσόπουλος Πέτρος 3130041
 */

package gr.pgetsos.helpers;

import gr.pgetsos.ApplicationFailedException;
import twitter4j.Status;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Locale;


public final class TweetCleaner {

    private static Set<String> stopWords;

    /**
     * Private empty constructor of Helper Class
     */
    private TweetCleaner(){}

    /**
     * Parse a List of Strings and clean them one by one and remove StopWords
     * @param tweets the List to clean
     * @return a new Map of Strings, now 100% more cleaned!
     */
    public static Map<String, Status> cleanData(List<Status> tweets) {
        //We use a Map so that duplicates are automatically removed!
        Map<String, Status> newTweets = new HashMap<>();

        for(Status tweet : tweets) {
            String clean = cleanTweet(tweet.getText());
            String[] words = clean.split(" ");
            StringBuilder sb = new StringBuilder(70);
            for (String word : words) {
                if(!stopWords.contains(word)){
                    sb.append(word).append(' ');
                }
            }
            newTweets.put(GreekStemmer.stemString(sb.toString().trim()), tweet);
        }

        return newTweets;
    }

    /**
     * Given a String, delete anything that is not a letter, including accents
     * @param tweet the String to be cleared
     * @return the cleaned String
     */
    public static String cleanTweet(String tweet){
        String cleanedTweet  = tweet;

        //Remove urls
        cleanedTweet = cleanedTweet.replaceAll("((www\\.[^\\s]+)|(https?://[^\\s]+))", " ");

        //Remove user names
        cleanedTweet = cleanedTweet.replaceAll("@[^\\s]+", " ");

        //Remove HashTags
        cleanedTweet = cleanedTweet.replaceAll("#[^\\s]+", " ");

        //Remove punctuation
        cleanedTweet = cleanedTweet.replaceAll("\\p{Punct}+", " ");

        //Remove Symbols
        cleanedTweet = cleanedTweet.replaceAll("\\P{L}+", " ");

        //Remove excessive spaces
        cleanedTweet = cleanedTweet.trim().replaceAll(" +"," ");

        //Remove Greek-French etc accents from letters
        cleanedTweet = Normalizer.normalize(cleanedTweet, Normalizer.Form.NFD);
        cleanedTweet = cleanedTweet.replaceAll("\\p{M}", "");

        //Convert Tweet to Upper Case
        cleanedTweet = cleanedTweet.toUpperCase(new Locale("el"));

        return cleanedTweet;
    }

    /**
     * Initialize the Set of StopWords from the given file
     * @throws ApplicationFailedException if the file reading fails
     */
    public static void initStopwords() throws ApplicationFailedException{
        stopWords = new HashSet<>(100);
        try {
            Path path = FileSystems.getDefault().getPath("src\\main\\resources", "greekstopwords.txt");
            Files.lines(path, StandardCharsets.UTF_8).forEach(stopWords::add);
        } catch (IOException io){
            throw new ApplicationFailedException(io);
        }
    }
}
