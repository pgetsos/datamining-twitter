/*
 * Created by PGETSOS on 03/01/2017. Γκετσόπουλος Πέτρος 3130041
 */

package gr.pgetsos.helpers;

import java.util.Arrays;
import java.util.List;

public final class GreekStemmer {

    /**
     * Private empty constructor of Helper Class
     */
    private GreekStemmer() {}

    /**
     * Given a String, split it to its words and Stem each one.
     * At the end "re-assemble" the stemmed parts
     * @param term the String to be stemmed
     * @return the final, stemmed String
     */
    public static String stemString(String term){
        StringBuilder sb = new StringBuilder(term.length());
        String[] words = term.split(" ");
        for (String word : words) {
            sb.append(stem(word)).append(' ');
        }
        return sb.toString().trim();
    }

    /**
     * Stems the given term to an unique discriminator
     * @param term The term that should be stemmed.
     * @return Discriminator for term
     */
    private static String stem(String term) {
        String newTerm = term;
        List<String> suffix3 = Arrays.asList("ΟΥΣ", "ΕΙΣ", "ΕΩΝ", "ΟΥΝ");
        List<String> suffix2 = Arrays.asList("ΟΣ", "ΗΣ", "ΕΣ", "ΩΝ", "ΟΥ", "ΟΙ", "ΑΣ", "ΩΣ", "ΑΙ", "ΥΣ", "ΟΝ", "ΑΝ", "ΕΙ", "ΙΑ");
        List<String> suffix1 = Arrays.asList("Α", "Η", "Ε", "Ω", "Υ", "Ο", "Ι");

        // Remove first level suffixes only if the term is 4 letters or more
        if (term.length() >= 4) {
            // Remove the 3 letter suffixes
            if (suffix3.contains(term.substring(term.length() - 3))) {
                newTerm = term.substring(0, term.length() - 3);
                // Remove the 2 letter suffixes
            } else if (suffix2.contains(term.substring(term.length() - 2))) {
                newTerm = term.substring(0, term.length() - 2);
                // Remove the 1 letter suffixes
            } else if (suffix1.contains(term.substring(term.length() - 1))) {
                newTerm = term.substring(0, term.length() - 1);
            }
        }
        return newTerm;
    }
}