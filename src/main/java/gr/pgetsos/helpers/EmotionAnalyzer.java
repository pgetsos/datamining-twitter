/*
 * Created by PGETSOS on 06/01/2017. Γκετσόπουλος Πέτρος 3130041
 */

package gr.pgetsos.helpers;

import gr.pgetsos.ApplicationFailedException;

import java.util.HashMap;
import java.util.Map;

public class EmotionAnalyzer {
    private static final String POSITIVE_FILE = "PosLex.xls";
    private static final String NEGATIVE_FILE = "NegLex.xls";

    private static Map<String, Integer> positive = new HashMap<>(400);
    private static Map<String, Integer> negative = new HashMap<>(400);

    /**
     * Private empty constructor of Helper Class
     */
    private EmotionAnalyzer(){}

    /**
     * Initialise the dictionaries of Positive and Negative words
     * from the given XLS files
     * @throws ApplicationFailedException if initialization(reading of files) fails
     */
    public static void initDictionaries() throws ApplicationFailedException {
        XLSReader.getPosNegWords(POSITIVE_FILE).forEach(string -> positive.put(string, 0));
        XLSReader.getPosNegWords(NEGATIVE_FILE).forEach(string -> negative.put(string, 0));
    }

    /**
     * Given a Tweet, analyze it see the emotion of it and return the result
     * @param tweet the Tweet to analyze
     * @return the final emotion of the Tweet
     */
    public static String analyzeTweet(String tweet){
        String[] words = tweet.split(" ");
        int counter = 0;
        for (String word : words) {
            if(positive.containsKey(word)){
                positive.put(word, positive.get(word)+1);
                counter++;
            }
            else if (negative.containsKey(word)){
                negative.put(word, negative.get(word)+1);
                counter--;
            }
        }
        return counter == 0 ? "Neutral" : counter > 0 ? "Positive" : "Negative";
    }

    /**
     * @return the Map of the positive words with their frequencies
     */
    public static Map<String, Integer> getPositive() {
        return positive;
    }

    /**
     * @return the Map of the negative words with their frequencies
     */
    public static Map<String, Integer> getNegative() {
        return negative;
    }
}
