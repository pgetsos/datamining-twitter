/*
 * Created by PGETSOS on 26/10/2016. Γκετσόπουλος Πέτρος 3130041
 */

package gr.pgetsos;

import gr.pgetsos.graphs.DataSetObject;
import gr.pgetsos.helpers.EmotionAnalyzer;
import gr.pgetsos.helpers.LoggerHelperClass;
import twitter4j.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This Class is responsible for anything related with the DataBase
 */
class DatabaseCreation {

    private static final Logger LOGGER = LoggerHelperClass.getNewLogger(DatabaseCreation.class);
    /**
     * JDBC and MySQL variables
     * Requires open MySQL server on host machine
     */
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_ADDRESS = "jdbc:mysql://localhost:3306/";
    private static final String DB_NAME = "Tweets";
    private static final String NO_SSL = "?useSSL=false&useUnicode=yes&characterEncoding=UTF-8";
    private static final String TABLE_NAME = "tweets";
    private static final String TABLE_NAME_WORDS = "wordsFreq";

    private static final String SQL_EXCEPTION = "Got an SQLException!";
    private static final String ALTER_TABLE = "ALTER TABLE ";
    private static final String COLLATE = "COLLATE utf8mb4_unicode_ci;";
    private static final String CHAR_SET = "CHARACTER SET utf8mb4 ";
    private static final String VARCHAR_NEW = "VARCHAR(191) ";
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private String userName;

    private String password;
    private Connection con;
    private Statement s;
    private PreparedStatement preparedStmt;
    private ResultSet rs;

    /**
     * Try to connect to the above specified Database.
     * If connection fails (non-existent Database), create it.
     * Also, change the default encoding to support various
     * UniCode characters Tweeps are often using (e.x. Heart)
     */
    DatabaseCreation(String userName, String password) throws ApplicationFailedException{
        try {
            this.password = password;
            this.userName = userName;
            Class.forName(JDBC_DRIVER);
            con = DriverManager.getConnection(DB_ADDRESS + DB_NAME + NO_SSL, userName, password);
            // Update encoding to support emoji and other UniCode characters
            s = con.createStatement();
            String updateEncoding1 = "SET NAMES utf8mb4;";
            String updateEncoding2 = "ALTER DATABASE "+ DB_NAME +" CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;";
            s.executeUpdate(updateEncoding1);
            s.executeUpdate(updateEncoding2);
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.SEVERE, "Class not found error (?!)", e);
            throw new ApplicationFailedException(e);
        } catch (SQLException e) {
            LOGGER.log(Level.INFO, "Database probably not found!", e);
            createDatabase();
        } finally {
            closeConnections();
        }
    }

    /**
     * Create a Database with the specified name, after connecting
     * to the MySQL server with the given credentials.
     */
    private void createDatabase() throws ApplicationFailedException{
        try {
            Class.forName(JDBC_DRIVER);
            con = DriverManager.getConnection(DB_ADDRESS, userName, password);
            s = con.createStatement();
            s.executeUpdate("CREATE DATABASE " + DB_NAME);
            String updateEncoding1 = "SET NAMES utf8mb4;";
            String updateEncoding2 = "ALTER DATABASE "+ DB_NAME +" CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;";
            s.executeUpdate(updateEncoding1);
            s.executeUpdate(updateEncoding2);
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.log(Level.SEVERE, "Fatal SQL error", e);
            closeConnections();
            throw new ApplicationFailedException(e);
        } finally {
            closeConnections();
        }
    }

    /**
     * Given an int for deletion or not of the old one,
     * create a Table in the Database to save Tweets.
     * Specifically, save the unique Tweet id, the user name, the
     * actual Tweet, the date it was created on, the number of
     * favourites and retweets it amassed, stemmed text and emotion.
     */
    void createTable(int delete) {
        try {
            if(delete==1) {
                preparedStmt = con.prepareStatement(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME));
                LOGGER.fine("\nDeleting the old data...");
                preparedStmt.execute();
            }
            LOGGER.fine("Creating new table...\n");
            s = con.createStatement();
            String sql = "CREATE TABLE  " + TABLE_NAME +
                    "(id BIGINT NOT NULL, " +
                    " user VARCHAR(255), " +
                    " username VARCHAR(255), " +
                    " tweet VARCHAR(255), " +
                    " date DATETIME, " +
                    " favs INTEGER , " +
                    " retweets INTEGER , " +
                    " searchTerm VARCHAR(150), " +
                    " clearText VARCHAR(150), " +
                    " emotion VARCHAR(8), " +
                    " PRIMARY KEY (id))";
            String encoding = ALTER_TABLE +
                    TABLE_NAME+" " +
                    "CONVERT TO CHARACTER SET utf8mb4 " +
                    COLLATE;
            String columnEncoding1 = ALTER_TABLE +
                    TABLE_NAME+" " +
                    "CHANGE tweet tweet " +
                    VARCHAR_NEW +
                    CHAR_SET +
                    COLLATE;
            String columnEncoding2 = ALTER_TABLE +
                    TABLE_NAME+" " +
                    "CHANGE user user " +
                    VARCHAR_NEW +
                    CHAR_SET +
                    COLLATE;
            s.executeUpdate(sql);
            s.executeUpdate(encoding);
            s.executeUpdate(columnEncoding1);
            s.executeUpdate(columnEncoding2);
        } catch (SQLException ignored){
            LOGGER.log(Level.FINER, "Probably no error, move on!", ignored);
        } finally {
            closeConnections();
        }
    }

    /**
     * Given an int for deletion or not of the old one, create a Table in the
     * Database to save Frequencies of words. Specifically, save the clear Text
     * of a Tweet, the date it was saved on, the number of times it was found,
     * the emotion of it and the term for which we save it.
     */
    void createTableForWords(int delete) {
        try {
            if(delete==1) {
                preparedStmt = con.prepareStatement(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME_WORDS));
                LOGGER.fine("\nDeleting the old data...");
                preparedStmt.execute();
            }
            LOGGER.fine("Creating new table for words...\n");
            s = con.createStatement();
            String sql = "CREATE TABLE  " + TABLE_NAME_WORDS +
                    " (clearText VARCHAR(150), " +
                    " date DATETIME , " +
                    " frequency INTEGER(10), " +
                    " emotion VARCHAR(150), " +
                    " searchTerm VARCHAR(150), " +
                    " PRIMARY KEY (clearText, date, searchTerm))";
            String encoding = ALTER_TABLE +
                    TABLE_NAME+" " +
                    "CONVERT TO CHARACTER SET utf8mb4 " +
                    COLLATE;
            String columnEncoding1 = ALTER_TABLE +
                    TABLE_NAME+" " +
                    "CHANGE clearText clearText " +
                    VARCHAR_NEW +
                    CHAR_SET +
                    COLLATE;
            s.executeUpdate(sql);
            s.executeUpdate(encoding);
            s.executeUpdate(columnEncoding1);
        } catch (SQLException ignored){
            LOGGER.log(Level.FINER, "Probably no error, move on!", ignored);
        } finally {
            closeConnections();
        }
    }

    /**
     * This method is responsible for saving Tweets of a specific Search Query.
     * It receives a collection of Tweets, for which it creates a new
     * PreparedStatement and executes them in batches to save time.
     * @param searchedTerm is the term we save for
     * @param tweets is the Collection of Tweets
     * @return a String to inform "the user" for the insertion
     */
    String saveToTable(String searchedTerm, Map<String, Status> tweets){
        try {
            // The MySQL Insert statement, with duplicate id check
            String query = "INSERT INTO " + TABLE_NAME + " (id, user, username, tweet, date, favs, retweets, searchTerm, clearText, emotion) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE id=id";
            preparedStmt = con.prepareStatement(query);
            int i = 0;

            // Iteration of the Collection
            for (Map.Entry<String, Status> entry : tweets.entrySet()) {
                Status tweet = entry.getValue();
                String emotion = EmotionAnalyzer.analyzeTweet(entry.getKey());

                preparedStmt.setLong (1, tweet.getId());
                preparedStmt.setString (2, tweet.getUser().getName());
                preparedStmt.setString (3, tweet.getUser().getScreenName());
                preparedStmt.setString (4, tweet.getText());
                preparedStmt.setTimestamp (5, new Timestamp(tweet.getCreatedAt().getTime()));
                preparedStmt.setInt (6, tweet.getFavoriteCount());
                preparedStmt.setInt (7, tweet.getRetweetCount());
                preparedStmt.setString (8, searchedTerm);
                preparedStmt.setString (9, entry.getKey());
                preparedStmt.setString (10, emotion);
                preparedStmt.addBatch();
                i++;
                /*
                 * Execute every 300 items, or at the very end
                 * (JDBC has a limit of 1000 I think, but better safe than sorry :) )
                */
                if (i % 300 == 0 || i == tweets.size()) {
                    preparedStmt.executeBatch();
                    LOGGER.log(Level.FINE,"Inserted up to number: "+i);
                }
            }
            return "succeeded :)";
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, SQL_EXCEPTION, e);
        } finally {
            closeConnections();
        }
        return "failed :(";
    }

    /**
     * This method is responsible for saving Frequencies of the Positive
     * and Negative words of a specific term for this day, overwriting old ones.
     * It receives a term and gets the frequencies from EmotionAnalyzer, and then
     * creates a new PreparedStatement and executes them in batches to save time.
     * @param searchTerm is the term we save for
     * @return a String to inform "the user" for the insertion
     */
    String saveToTableFrequencies(String searchTerm){
        try {
            // The MySQL Insert statement, with duplicate id check
            String query = "INSERT INTO " + TABLE_NAME_WORDS + " (clearText, date, frequency, emotion, searchTerm) " +
                    "VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE frequency=?";
            preparedStmt = con.prepareStatement(query);
            Calendar cal = new GregorianCalendar();
            cal.add(Calendar.DAY_OF_MONTH, -1);
            Date beginning = cal.getTime();
            int i = 0;
            Map<String, Integer> positive = EmotionAnalyzer.getPositive();
            Map<String, Integer> negative = EmotionAnalyzer.getNegative();
            // Iteration of the Collection
            for (Map.Entry<String, Integer> entry : positive.entrySet()) {
                preparedStmt.setString (1, entry.getKey());
                preparedStmt.setTimestamp(2, new Timestamp(beginning.getTime()));
                preparedStmt.setInt (3, entry.getValue());
                preparedStmt.setInt (6, entry.getValue());
                preparedStmt.setString (4, "Positive");
                preparedStmt.setString (5, searchTerm);
                preparedStmt.addBatch();
                i++;
                /*
                 * Execute every 300 items, or at the very end
                 * (JDBC has a limit of 1000 I think, but better safe than sorry :) )
                */
                if (i % 300 == 0 || i == positive.size()) {
                    preparedStmt.executeBatch();
                    LOGGER.log(Level.FINE,"Inserted up to positive word number: "+i);

                }
            }

            i=0;
            for (Map.Entry<String, Integer> entry : negative.entrySet()) {
                preparedStmt.setString (1, entry.getKey());
                preparedStmt.setTimestamp(2, new Timestamp(beginning.getTime()));
                preparedStmt.setInt (3, entry.getValue());
                preparedStmt.setString (4, "Negative");
                preparedStmt.setString (5, searchTerm);
                preparedStmt.addBatch();
                i++;
                /*
                 * Execute every 300 items, or at the very end
                 * (JDBC has a limit of 1000 I think, but better safe than sorry :) )
                */
                if (i % 300 == 0 || i == negative.size()) {
                    preparedStmt.executeBatch();
                    LOGGER.log(Level.FINE,"Inserted up to negative word number: " + i);
                }
            }
            return "succeeded :)";
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, SQL_EXCEPTION, e);
        } finally {
            closeConnections();
        }
        return "failed :(";
    }

    /**
     * This method is responsible for getting the various small stats from the
     * DataBase and return them in a ArrayList of Strings
     * @return the stats in an List
     */
    List<String> getStats(String hashTag1, String hashTag2, String mention1, String mention2){
        List<String> stats = new ArrayList<>();
        try{
            String query = "Select count(*), searchTerm FROM " + TABLE_NAME + " where searchTerm like ? OR searchTerm like ? Group By searchTerm";
            preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, "%" + mention1 + "%");
            preparedStmt.setString(2, "%" + mention2 + "%");
            rs = preparedStmt.executeQuery();
            stats.add("Number of Tweets by search term (excluding those with both HashTags)\n");
            while(rs.next()) {
                stats.add(rs.getInt(1) + " Tweets for term: " + rs.getString(2));
            }
            query = "Select count(*), searchTerm FROM "+TABLE_NAME+" where (tweet not like ? and searchTerm like ?) OR (tweet not like ? and searchTerm like ?) Group By searchTerm";
            preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, hashTag1);
            preparedStmt.setString(2, hashTag2);
            preparedStmt.setString(3, hashTag2);
            preparedStmt.setString(4, hashTag1);
            rs = preparedStmt.executeQuery();
            while(rs.next()){
                stats.add(rs.getInt(1)+" Tweets for term: "+rs.getString(2));
            }
        }
        catch (SQLException e) {
            LOGGER.log(Level.WARNING, SQL_EXCEPTION, e);
        } finally {
            closeConnections();
        }
        return stats;
    }

    /**
     * This method is responsible for returning a List of DataSetObjects,
     * that contains the number of Positive and Negative Tweets of the term
     * given, by date, for the last 7 days even if no entries exist for a day.
     * @param term the term it will search for in the DataBase
     * @return the stats in an List of DataSetObjects
     */
    List<DataSetObject> getStats(String term){
        List<DataSetObject> stats = new ArrayList<>();
        Map<String, Integer> pos = new HashMap<>(50);
        Map<String, Integer> neg = new HashMap<>(50);
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DAY_OF_MONTH, -7);
        Date beginning = cal.getTime();

        try{
            String query = "Select count(*), emotion, date(date) from " + TABLE_NAME + " where emotion != 'Neutral' and date(date) >= ? and date(date) < ? and searchTerm like ? group by date(date), emotion";
            preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, sdf.format(beginning.getTime()));
            preparedStmt.setString(2, sdf.format(new Date().getTime()));
            preparedStmt.setString(3, "%" + term + "%");
            rs = preparedStmt.executeQuery();

            while(rs.next()) {
                if("Positive".equals(rs.getString(2))){
                    pos.put(rs.getString(3), rs.getInt(1));
                }
                else {
                    neg.put(rs.getString(3), rs.getInt(1));
                }
            }
            for (Map.Entry<String, Integer> entry : pos.entrySet()) {
                stats.add(new DataSetObject(entry.getKey(), entry.getValue(), neg.getOrDefault(entry.getKey(), 0)));
                neg.remove(entry.getKey());
            }
            if(neg.size()!=0){
                for (Map.Entry<String, Integer> entry : neg.entrySet()) {
                    stats.add(new DataSetObject(entry.getKey(), 0, neg.get(entry.getKey())));
                }
            }
            if(stats.size()!=7) {
                for(int i = 0; i < 7; i++){
                    if(!stats.contains(new DataSetObject(sdf.format(cal.getTime()), 0, 0))){
                        stats.add(new DataSetObject(sdf.format(cal.getTime()), 0, 0));
                    }
                    cal.add(Calendar.DAY_OF_MONTH, 1);
                }
            }
            return stats;
        }
        catch (SQLException e) {
            LOGGER.log(Level.WARNING, SQL_EXCEPTION, e);
        } finally {
            closeConnections();
        }
        return stats;
    }

    /**
     * This method is responsible for returning a List of DataSetObjects,
     * that contains the number of Tweets per term for the last 7 days
     * @return the stats in an List of DataSetObjects
     */
    List<DataSetObject> getWStats(){
        List<DataSetObject> stats = new ArrayList<>();
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DAY_OF_MONTH, -7);
        Date beginning = cal.getTime();

        try{
            String query = "Select count(*), searchTerm from " + TABLE_NAME + " where date(date) >= ? and date(date) < ? group by searchTerm";
            preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, sdf.format(beginning.getTime()));
            preparedStmt.setString(2, sdf.format(new Date().getTime()));
            rs = preparedStmt.executeQuery();

            while(rs.next()) {
                stats.add(new DataSetObject(rs.getString(2), rs.getInt(1)));
            }
            return stats;
        }
        catch (SQLException e) {
            LOGGER.log(Level.WARNING, SQL_EXCEPTION, e);
        } finally {
            closeConnections();
        }
        return stats;
    }

    /**
     * This method is responsible for returning a List of DataSetObjects,
     * that contains the number of Positive Tweets per term for the last 7 days
     * @return the stats in an List of DataSetObjects
     */
    List<DataSetObject> getWPosStats(){
        List<DataSetObject> stats = new ArrayList<>();
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DAY_OF_MONTH, -7);
        Date beginning = cal.getTime();

        try{
            String query = "Select count(*), searchTerm from " + TABLE_NAME + " where date(date) >= ? and date(date) < ? and emotion = 'Positive' group by searchTerm";
            preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, sdf.format(beginning.getTime()));
            preparedStmt.setString(2, sdf.format(new Date().getTime()));
            rs = preparedStmt.executeQuery();

            while(rs.next()) {
                stats.add(new DataSetObject(rs.getString(2), rs.getInt(1)));
            }
            return stats;
        }
        catch (SQLException e) {
            LOGGER.log(Level.WARNING, SQL_EXCEPTION, e);
        } finally {
            closeConnections();
        }
        return stats;
    }

    /**
     * Checks the DataBase for the existence of a table
     * @return true or false, depending if it exists
     */
    boolean checkTableExistence(){
        boolean toReturn = false;
        try {
            s = con.createStatement();
            rs = s.executeQuery("SHOW TABLES LIKE '"+TABLE_NAME+"';");
            toReturn = rs.next();
        } catch(SQLException e){
            LOGGER.log(Level.WARNING, SQL_EXCEPTION, e);
        } finally {
            closeConnections();
        }
        return toReturn;
    }

    /**
     * This method is responsible for closing all the connections to the DataBase
     */
    private void closeConnections() {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Got an SQLException closing ResultSet!", e);
        }
        try {
            if (s != null) {
                s.close();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Got an SQLException closing Statement!", e);
        }
        try {
            if (preparedStmt != null) {
                preparedStmt.close();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Got an SQLException closing PreparedStatement!", e);
        }
    }

    /**
     * This method is responsible for closing the main connection to the DataBase
     */
    void closeCon(){
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Got an SQLException closing Connection!", e);
        }
    }
}
