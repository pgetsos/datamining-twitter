/*
 * Created by PGETSOS on 25/10/2016. Γκετσόπουλος Πέτρος 3130041
 */

package gr.pgetsos;

import gr.pgetsos.helpers.LoggerHelperClass;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RetrieveTweets {

    /**
     * Private empty constructor of Main Class
     */
    private RetrieveTweets(){}

    /**
     * Create a menu for the user to choose what (s)he wants to do with
     * the program, and then call TweetSearchEngine with the choice as argument.
     * Also asks for the MySQL server credentials on boot
     * @param args the arguments of the execution
     */
    public static void main(String[] args) {
        Logger logger = LoggerHelperClass.getNewLogger(RetrieveTweets.class);
        Scanner reader = new Scanner(System.in, StandardCharsets.UTF_8.name());
        TweetSearchEngine tse = new TweetSearchEngine();

        logger.log(Level.FINE, "Enter server username");
        String username = reader.next();
        logger.log(Level.FINE, "Enter server username");
        String password = reader.next();
        int input = -1;
        while(input!=5) {
            logger.log(Level.FINE, "\nWhat would you like to do?\n");
            logger.log(Level.FINE, "1. Download new Tweets (for yesterday) and delete the old ones (for #ND, #SYRIZA, @atsipras)" +
                    "\n2. Download additional Tweets (for yesterday) (for #ND, #SYRIZA, @atsipras)" +
                    "\n3. Show small interesting stats for the saved Tweets" +
                    "\n4. Extract detailed interesting stats for the saved Tweets" +
                    "\n5. Exit");
            input = reader.nextInt();
            if(input < 1 || input > 5){
                logger.log(Level.FINE, "*****Please, enter a valid input (1-5)!*****\n");
                continue;
            }
            if(input == 5)
                break;
            if (input == 4){
                while (input < 6 || input > 10) {
                    logger.log(Level.FINE, "1. Show number of tweets per term (total, last 7 days)" +
                            "\n2. Show Positive/Negative tweets per term (last 7 days)" +
                            "\n3. Show Average Positive Tweets per term (total, last 7 days)" +
                            "\n4. Show Standard Deviation of Pos/Neg Tweets per term (total, last 7 days)" +
                            "\n5. Back");
                    input = reader.nextInt() + 5;
                    if(input < 6 || input > 10){
                        logger.log(Level.FINE, "*****Please, enter a valid input (1-5)!*****\n");
                    }
                }
                if(input == 10){
                    continue;
                }
            }
            try {
                tse.getTweets(input, username, password);
            } catch (ApplicationFailedException e) {
                logger.log(Level.SEVERE, "Severe error-EXITING", e);
                System.exit(1);
            }
        }
        System.exit(0);
    }
}