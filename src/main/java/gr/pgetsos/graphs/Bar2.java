package gr.pgetsos.graphs;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DefaultKeyedValues2DDataset;
import org.jfree.data.general.KeyedValues2DDataset;
import org.jfree.ui.ApplicationFrame;

import java.awt.*;
import java.util.List;

/**
 * Created by pgetsos on 1/27/2017. Γκετσόπουλος Πέτρος 3130041
 */
public class Bar2 extends ApplicationFrame {

    /**
     * Creates a new Bar Graph.
     * @param title the frame title.
     */
    public Bar2(String title, String[] chartVals, List<DataSetObject> dataSet, int mode) {

        super(title);
        CategoryDataset dataset = mode == 0 ? createDataset(dataSet) : createDatasetW(dataSet);

        // create the chart...
        JFreeChart chart = ChartFactory.createBarChart(
                chartVals[0],
                chartVals[1],     // domain axis label
                chartVals[2], // range axis label
                dataset,         // data
                PlotOrientation.VERTICAL,
                true,            // include legend
                true,            // tooltips
                false            // urls
        );
        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.BLACK);

        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(0, Color.GREEN);
        renderer.setSeriesPaint(1, Color.RED);
        renderer.setSeriesPaint(2, Color.BLUE);
        renderer.setDrawBarOutline(false);
        renderer.setItemMargin(0);

        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(620, 600));
        setContentPane(chartPanel);
    }

    /**
     * Creates a new DataSet from the data it receives with the average of the
     * Tweets (to be used in "Average per Day" Graph)
     * @param dataSet the data we insert
     * @return a DataSet for the graph creation
     */
    private KeyedValues2DDataset createDataset(List<DataSetObject> dataSet) {
        DefaultKeyedValues2DDataset data = new DefaultKeyedValues2DDataset();
        for (DataSetObject setObject : dataSet) {
            data.addValue((double)setObject.getPositive()/7, "Positive", setObject.getSearchTerm());
        }
        return data;
    }

    /**
     * Creates a new DataSet from the data it receives
     * @param dataSet the data we insert
     * @return a DataSet for the graph creation
     */
    private KeyedValues2DDataset createDatasetW(List<DataSetObject> dataSet) {
        DefaultKeyedValues2DDataset data = new DefaultKeyedValues2DDataset();
        for (DataSetObject setObject : dataSet) {
            data.addValue((double)setObject.getPositive(), "Tweets", setObject.getSearchTerm());
        }
        return data;
    }
}