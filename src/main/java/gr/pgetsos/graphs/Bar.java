package gr.pgetsos.graphs;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.StackedBarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DefaultKeyedValues2DDataset;
import org.jfree.data.general.KeyedValues2DDataset;
import org.jfree.ui.ApplicationFrame;

import java.awt.*;
import java.util.Comparator;
import java.util.List;

/**
 * Created by pgetsos on 1/21/2017. Γκετσόπουλος Πέτρος 3130041
 */
public class Bar extends ApplicationFrame {

    /**
     * Creates a new Bar(Population) Graph.
     * @param title the frame title.
     */
    public Bar(String title, String[] chartVals, List<DataSetObject> dataSet) {

        super(title);
        CategoryDataset dataset = "SD".equals(chartVals[2]) ? createDatasetD(dataSet) : createDataset(dataSet);

        // create the chart...
        JFreeChart chart = ChartFactory.createStackedBarChart(
                chartVals[0],
                chartVals[1],     // domain axis label
                chartVals[2], // range axis label
                dataset,         // data
                PlotOrientation.HORIZONTAL,
                true,            // include legend
                true,            // tooltips
                false            // urls
        );

        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.CYAN);
        plot.setRangeGridlinePaint(Color.BLACK);

        StackedBarRenderer renderer = (StackedBarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(0, Color.RED);
        renderer.setSeriesPaint(1, Color.GREEN);
        renderer.setDrawBarOutline(true);
        renderer.setItemMargin(0);

        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(690, 620));
        setContentPane(chartPanel);
    }

    /**
     * Creates a DataSet sorted by Date from the data it receives
     * @param dataSet the data to create a DataSet from
     * @return a DataSet to be used in the graph
     */
    private KeyedValues2DDataset createDataset(List<DataSetObject> dataSet) {
        DefaultKeyedValues2DDataset data = new DefaultKeyedValues2DDataset();
        dataSet.sort(Comparator.comparing(DataSetObject::getDate));
        for (DataSetObject setObject : dataSet) {
            data.addValue(setObject.getNegative()*-1, "Negative", setObject.getDate());
            data.addValue(setObject.getPositive(), "Positive", setObject.getDate());
        }
        return data;
    }

    /**
     * Creates a DataSet sorted by term from the data it receives.
     * It is used for Double type of inputs
     * @param dataSet the data to create a DataSet from
     * @return a DataSet to be used in the graph
     */
    private KeyedValues2DDataset createDatasetD(List<DataSetObject> dataSet) {
        DefaultKeyedValues2DDataset data = new DefaultKeyedValues2DDataset();
        dataSet.sort(Comparator.comparing(DataSetObject::getSearchTerm));
        for (DataSetObject setObject : dataSet) {
            data.addValue(setObject.getNegativeD()*-1, "Negative", setObject.getSearchTerm());
            data.addValue(setObject.getPositiveD(), "Positive", setObject.getSearchTerm());
        }
        return data;
    }
}