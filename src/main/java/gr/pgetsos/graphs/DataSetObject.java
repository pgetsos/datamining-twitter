/*
 * Created by pgetsos on 1/25/2017. Γκετσόπουλος Πέτρος 3130041
 */

package gr.pgetsos.graphs;

/**
 * This Class is used for the transactions between the DataBase and the Graphs
 */
public class DataSetObject {
    private String date;
    private String searchTerm;
    private int positive;
    private int negative;
    private double positiveD;
    private double negativeD;

    /**
     * Constructor containing a String and a single Integer argument
     */
    public DataSetObject(String searchTerm, int positive) {
        this.searchTerm = searchTerm;
        this.positive = positive;
    }

    /**
     * Constructor containing a String and 2 Integer arguments
     */
    public DataSetObject(String date, int positive, int negative) {
        this.date = date;
        this.positive = positive;
        this.negative = negative;
    }

    /**
     * Constructor containing a String and 2 Double arguments
     */
    public DataSetObject(String searchTerm, double positiveD, double negativeD) {
        this.searchTerm = searchTerm;
        this.positiveD = positiveD;
        this.negativeD = negativeD;
    }

    /**
     * @return the positive Tweets in Double type
     */
    double getPositiveD() {
        return positiveD;
    }

    /**
     * @return the negative Tweets in Double type
     */
    double getNegativeD() {
        return negativeD;
    }

    /**
     * @return the Date saved for this object
     */
    String getDate() {
        return date;
    }

    /**
     * @return the SearchTerm of the object
     */
    String getSearchTerm() {
        return searchTerm;
    }

    /**
     * @return the positive Tweets in Integer type
     */
    public int getPositive() {
        return positive;
    }

    /**
     * @return the negative Tweets in Integer type
     */
    public int getNegative() {
        return negative;
    }

    /**
     * @param o the object to check for equality
     * @return if both the date and the search terms are equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        DataSetObject that = (DataSetObject) o;

        return date.equals(that.date) && searchTerm.equals(that.searchTerm);
    }

    /**
     * @return a HashCode based on the 2 Strings of the class
     */
    @Override
    public int hashCode() {
        return date.hashCode()+searchTerm.hashCode();
    }
}
